import express, { Request, Response, NextFunction } from 'express';
import fs from 'fs';

export default () => {
  // Instantiate Express
  const app = express();

  // Set Response
  app.use('/', (req: Request, res: Response, next: NextFunction) => {
    try {
      const response = fs.readFileSync('/tmp/serve.html', 'utf-8');
      res.send(response);
    } catch (err) {
      res.send('<h1>Server is Running</h1>');
    }
  });

  app.listen(8080, () => {
    console.log('Successfully started the web server...');
  });
};