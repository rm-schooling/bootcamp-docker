import express, { Request, Response, NextFunction } from 'express';

export default () => {
  // Instantiate Express
  const app = express();

  // Set Response
  app.use('/', (req: Request, res: Response, next: NextFunction) => {
    res.send('<h1>Your great app is running just fine</h1>');
  });

  app.listen(8080, () => {
    console.log('Successfully started the web server...');
  });
};